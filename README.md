Build the box: `vagrant up`

Login: `vagrant ssh`

Run Jekyll: `cd /vagrant && bundle exec jekyll serve --host=0.0.0.0 --drafts`
